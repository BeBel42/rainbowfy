#ifndef COLORDEPTH_H_
#define COLORDEPTH_H_

#include <functional>
#include <string>

class ColorDepth {
	private:
		using getter_type = std::function<std::string(int, int, int)>;

	public:

		// (0 <= r, g, b <= 5)
		static std::string get_color_code_8(int r, int g, int b) noexcept {
			using std::to_string;
			return "\033[38:5:" + to_string(16 + 36 * r + 6 * g + b) + 'm';
		}
		// (0 <= r, g, b <= 255)
		static std::string get_color_code_24(int r, int g, int b) noexcept {
			using std::to_string;
			return "\033[38;2;" + to_string(r) + ';' + to_string(g) + ';' + to_string(b) + 'm';
		}

		bool operator==(const ColorDepth& obj) const noexcept;

		ColorDepth(unsigned bit_depth, unsigned max_val, getter_type get_color_code) noexcept;
		unsigned bit_depth;
		unsigned max_val;
		getter_type get_color_code;
};



#endif // COLORDEPTH_H_
