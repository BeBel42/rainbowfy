#ifndef ARGUMENTS_H_
#define ARGUMENTS_H_

#include "ColorDepth.hpp"

class Arguments {
	private:
	public:
		int direction; // 1 -> forward, -1 -> backwards
		float speed;
		ColorDepth depth;

		Arguments(int argc, char** argv) noexcept;
};

#endif // ARGUMENTS_H_
