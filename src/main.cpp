#include <iostream>
#include <unistd.h>
#include <cctype>
#include <string>
#include <cstdio>
#include <functional>
#include <type_traits>
#include <array>
#include <cmath>
#include "Arguments.hpp"

constexpr auto buffer_size = 512u;

// get values (between 0 and 5 included) according to given index
static constexpr unsigned new_value(unsigned index, unsigned max_val) noexcept
{
	switch(index / (max_val + 1)) {
		case 0:
			return index;
		case 1:
			return max_val;
		case 2:
			return max_val;
		case 3:
			return max_val - (index - (max_val + 1) * 3);
		default:
			return 0;
	}
}

int main(int argc, char** argv) noexcept
{
	const auto istty = static_cast<bool>(isatty(fileno(stdin)));
	const auto reset_color = std::string("\033[0m");

	const auto args = Arguments(argc, argv);
	const auto max_index = static_cast<unsigned>(args.depth.max_val * 6 + 1);

	// default values of rgb
	const auto ri_d = 0.f;
	const auto gi_d = static_cast<float>(max_index) / 3 * 2;
	const auto bi_d = static_cast<float>(max_index) / 3;

	// rgb variables
	auto ri = ri_d;
	auto gi = gi_d;
	auto bi = bi_d;

	// will be filled with stdin
	auto buffer = std::array<char, buffer_size>();
	// n of chars put in buffer
	auto chars_read = 0;
	// flag to see if inside of color sequence
	auto is_in_color_code = false;

	// for each stdin read, iterate over buffer and ouptput colored text
	while ((chars_read = read(0, buffer.data(), buffer_size)) > 0)
	{
		using buf_size_nconst_t = std::remove_const_t<decltype(buffer_size)>;
		for (buf_size_nconst_t i = 0; i < static_cast<buf_size_nconst_t>(chars_read); i++) {
			// character read from buffer
			const auto c = buffer[i];

			// checking if beginning of color sequence
			if (c == '\033') is_in_color_code = true;

			// skipping non-printable characters
			if (is_in_color_code || (!std::isspace(c) && !std::isprint(c))) {
				if (c == 'm') is_in_color_code = false;
				continue;
			}

			// adding some offset on a new line
			if (c == '\n') {
				// offset that will be incremented every line
				static auto offset = 0.f;

				// applying offset to indexes
				offset = std::fmod(offset + args.speed, max_index);
				ri = std::fmod(ri_d - offset * args.direction + max_index, max_index);
				gi = std::fmod(gi_d - offset * args.direction + max_index, max_index);
				bi = std::fmod(bi_d - offset * args.direction + max_index, max_index);

				// resetting colors before next user input
				if (istty) std::cout << reset_color;

				std::cout << '\n';
				continue;
			}

			// updating the values of rgb
			const auto r = new_value(ri, args.depth.max_val);
			const auto g = new_value(gi, args.depth.max_val);
			const auto b = new_value(bi, args.depth.max_val);

			// incrementing the rgb values
			if (ri >= max_index - 1) ri = std::fmod(ri, max_index - 1); else ri += args.speed;
			if (gi >= max_index - 1) gi = std::fmod(gi, max_index - 1); else gi += args.speed;
			if (bi >= max_index - 1) bi = std::fmod(bi, max_index - 1); else bi += args.speed;

			// displaying colored character
			std::cout << args.depth.get_color_code(r, g, b) << c;
		}
	}

	// resetting colors
	std::cout << reset_color;
}
