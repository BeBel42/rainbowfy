#include "Arguments.hpp"
#include "ColorDepth.hpp"
#include <bits/getopt_core.h>
#include <string>
#include <vector>
#include <unistd.h>
#include <iostream>
#include <getopt.h>
#include <array>
#include <cstdlib>

const auto depth24 = ColorDepth(24, 255, ColorDepth::get_color_code_24);
const auto depth8 = ColorDepth(8, 5, ColorDepth::get_color_code_8);

const std::string help_message = R"(
rainbowfy - colors output in a rainbow palette
    -h, --help      prints help message
    -d, --depth     specify bit depth (8 or 24)
    -r, --reverse   reverse the rainbow direction
    -f, --frequency changes rainbow frequency (any positive nonzero float)

example:
	echo "This is an example!" | rainbowfy -d 8 -r -s 2.5
)";

const auto short_options =  std::string("hrf:d:");
const auto long_options = std::array{
	option{"help", 0, nullptr, 'h'},
	option{"reverse", 0, nullptr, 'r'},
	option{"frequency", 0, nullptr, 'f'},
	option{"depth", 0, nullptr, 'd'},
	option{nullptr, 0, nullptr, 0}
};

Arguments::Arguments(int argc, char** argv) noexcept:
	direction(-1),
	speed(1),
	depth(depth24)
{
	// reading COLORTERN env variable
	const auto color_term = std::getenv("COLORTERM");
	if (color_term && color_term == std::string("8bit"))
		depth = depth8;

	// parsing arguments
	int opt;
	while ((opt = getopt_long(argc, argv, short_options.c_str(), long_options.data(), nullptr)) != -1)
	{
		switch(opt) {
			case 'r':
				direction = 1;
				break;
			case 'f':
				speed = std::atof(optarg);
				if (speed <= 0) {
					std::cerr << "Invalid frequency: " << optarg << std::endl;
					exit(1);
				}
				break;
			case 'd':
				switch(std::atoi(optarg)) {
					case 8:
						depth = depth8;
						break;
					case 24:
						depth = depth24;
						break;
					default:
						std::cerr << "Invalid bit depth: " << optarg << std::endl;
						exit(1);
				}
				break;
			case 'h':
				std::cout << help_message;
				exit(0);
				break;
			default:
				std::cerr << help_message;
				exit(1);
		}
	}

	// because there are too many colors in 24 bits
	if (depth == depth24)
		speed *= 17;
}
