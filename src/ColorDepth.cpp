#include "ColorDepth.hpp"

// (0 <= r, g, b <= 5)
static inline std::string get_color_code_8(int r, int g, int b) noexcept {
	using std::to_string;
	return "\033[38:5:" + to_string(16 + 36 * r + 6 * g + b) + 'm';
}
// (0 <= r, g, b <= 255)
static inline std::string get_color_code_24(int r, int g, int b) noexcept {
	using std::to_string;
	return "\033[38;2;" + to_string(r) + ';' + to_string(g) + ';' + to_string(b) + 'm';
}


bool ColorDepth::operator==(const ColorDepth& obj) const noexcept{
	return (obj.bit_depth == bit_depth && obj.max_val == max_val);
}

ColorDepth::ColorDepth(unsigned bit_depth, unsigned max_val, getter_type get_color_code) noexcept:
	bit_depth(bit_depth),
	max_val(max_val),
	get_color_code(get_color_code)
{}
